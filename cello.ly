\include "def.ly"

celloOne = {
	\set Staff.midiInstrument = #"cello"
	\set Staff.instrumentName = #"Cello I"
	
	\clef treble
	\key d \minor
	\time 3/4	
	
	e''2.\p\(^\markup{\halign #-0.1 \tiny \italic "lento e espressivo"}
	fis''2.
	e''2.~
	e''2.\)
	
	e''2.\(^\markup{\halign #-0.4 \tiny \italic "più mosso"}
	fis''2.
	e''2.~
	e''2.\)
	
	e''2.\(
	fis''2.
	e''2.~
	e''2.\)	
	
	e''2.\>
	d''2.
	c''2.~
	c''2.\!\pp\fermata
	
	R2.*16
	
	\repeat volta 2 
	{
		g'4\(^\markup{\tiny \italic "eroico"} bes' d'' 
		g'4 bes' d'' 
		g'4 bes' d'' 
		g'4 bes' d''\)
		
		d''4\( bes' g'
		d''4 bes' g'
		d''4 bes' g'
		d''4 bes' g'\)
		
		es''4_\( bes' g'
		es''4 bes' g'
		es''4 bes' g'		
		es''4 bes' g'\)
	}
	\alternative 
	{
		{
			a'2.
			g'2.
			f'2.
			e'2.
		}
		{
			r2.^\markup{\tiny \italic ritardando}
			R2.*2
			r4\fermata f''2\pp\fermata
		}		
	}
	
	R2.*17	
	g'4\( a' bes'
	c''2.~
	c''2.\)
	
	bes'2.~
	bes'2.
	a'2.~
	a'2.
	
	d'2.
	d'4 e' f'\(
	g'2.
	f'2.
	
	c''2.\fermata\)
	f'2.\(
	e'2.
	d'2.~
	
	d'2.\)
	d'2.\(
	c'2.
	bes2.~
	
	bes2.\)
	R2.*4
	f'4 a' c''\(
	f'4 a' d''
	
	e'4 g' bes'\fermata
	a'2.\)
	
	\time 4/4
	R1*3
	\repeat volta 2 
	{
		R1*3
	}
	\alternative
	{
		{
			R1*1
		}
		{
			R1*1
		}
	}
	
	\time 3/4 
	
	d'2 f'4\(
	a'2 g'4
	f'2 c'4
	c'2.\)
	
	d'2 f'4\(
	g'2 bes'4
	f'2 e'4
	a'2 e'4\)
	
	d'2.~\(
	d'2.
	d'2.~
	d'2 d'4
	
	e'2.~
	e'2.
	f'2.
	g'2.\)
	
	d'4\(^\markup{\tiny \italic allegro } e' f'\)
	d'4\( e' f'\)
	d'4\( e' f'\)
	d'4\( e' f'\)

	c'4\( e' f'\)
	c'4\( e' f'\)
	c'4\( e' f'\)	
	c'4\( e' f'\)
	
	bes4\(^\markup{\tiny \italic ritard.} c' d'\)
	bes4\( c' d'
	a2.~
	a2.\)
	
	r2.
	r2.
	d'4.-> r4.
}

celloTwo = {
	\set Staff.midiInstrument = #"cello"
	\set Staff.instrumentName = #"Cello II"
	
	\clef bass
	\key d \minor
	\time 3/4
	
	R2.*8
	
	a2.\(\p
	g2.
	a2.~
	a2.\)
	
	a2.\>
	g2.~
	g2.~
	g2.\!\pp\fermata
	
	R2.*16
	
	\repeat volta 2 
	{
		g2.~
		g2.~
		g2.~
		g2.
		
		f2.~
		f2.
		c'2.~
		c'2.
		
		bes2.~
		bes2.~
		bes2.~
		bes2 g4
	}
	\alternative
	{
		{
			a2.\(
			bes2.
			a2.~ 
			a2.\)
		}
		{
			d2._\subMF_\>
			bes2.~
			bes2.
			f2.\!\pp\fermata
		}
	}

	R2.*17	
	
	\clef treble
	
	c'2.\(
	f'2.
	f'2 f'4\)
	g'2.\(
	
	d'2 d'4
	e'2.\)
	e'4\( d' c'\)
	\clef bass
	g,4\( bes, d
	
	f4 g a
	bes2.
	g2.
	c'2.\)
	
	R2.*4
	
	d2.\(
	e2.
	f2.~
	f2.\)
	
	f2.\(
	g2.
	a2.~
	a2.\)
	
	f2.\(
	d2.
	c2.\fermata
	c'2.\)
	
	\time 4/4 
	R1*3
	\repeat volta 2
	{
		R1*3
	}
	\alternative 
	{
		{
			R1*1
		}
		{
			R1*1
		}
	}
	
	\time 3/4
	
	R2.*10
	a2.~
	a2 a4
	c'2.~ 
	
	c'2.
	bes2.\(
	c'2.\)
	a2 a4
	
	f2 f4
	f2 f4
	a2 a4
	g2 g4
	
	g2 g4
	f2 f4
	f2 f4
	R2.*1
	
	R2.*5
	a4.-> r4.	
}