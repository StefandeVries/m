\version "2.16.0"
\pointAndClickOff 

\header {
		title = "M"
		composer = "Stefan de Vries"
		tagline = \markup{\teeny " This composition is licensed under a Creative Commons Attribution-ShareAlike 3.0 Unported License."}
}

\include "cello.ly"
\include "piano.ly"

\score {
		<<
			\new StaffGroup 
			<<
 				\new Staff = "cello1" \celloOne
				\new Staff = "cello2" \celloTwo
			>>
			\new PianoStaff
			<<
				\set PianoStaff.instrumentName = #"Piano"
				\new Staff = "pianoRight" \pianoRight
				\new Staff = "pianoLeft" \pianoLeft
			>>
				
		>>
		\midi{}
		\layout{}
}