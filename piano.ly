\include "def.ly"

pianoRight = {
	\clef treble
	\key d \minor
	\time 3/4
	
	R2.*32
	
	\repeat volta 2
	{
		r2.
		g'4-> r2
		
		r2.
		f'4-> r2
		
		r2.
		r2.
		r2.
		\acciaccatura bes'16 a'2.\laissezVibrer
		
		r2.
		<bes' es'>4-> r2
		r2.
		<bes' es'>4-> r2
		
	}
	\alternative 
	{
		{
			d''2.\(
			f''2.
			a''2.
			c'''2.\)
		}
		{
			d'4\( f' a'
			d''4 bes' g'
			f'2 e'4
			<f' a' c''>2.\)\fermata
		}
	}
	
	f'2.~
	f'2.
	g'2.~
	g'2.
	
	<f' a'>2.
	<d' f' a'>2.
	<c' f'>2.\(
	c''2.\)
	
	d''4\( c'' bes'
	a' g' f'
	<e' g'>2.\)
	<d' f'>2 c'8 d'
	
	<f' c''>2 <f' c''>4
	<f' d''>2 <f' d''>4
	<f' bes' d''>2 <f' bes' d''>4
	
	c'4\( f' g'
	<c' f'>2.\)
	g'4 a' bes'\(
	a2.~
	
	a2.\)
	R2.*3
	
	R2.*4
	
	r2.
	<f' a'>2.\fermata
	R2.*14
	r2 <e' g'>4\fermata
	
	f'2.
	\time 4/4
	<d' f' a'>2. <c' e' g'>4
	<d' f' a' d''>1\fermata\arpeggio~
	<d' f' a' d''>1
	
	\repeat volta 2 {
		R1*3
	}
	
	\alternative {
			{
				R1*1
			}
			{
				R1*1
			}
	}
	
	\time 3/4
	\clef bass
	r4 <d f>2
	r4 <c e>2
	r4 <d f>2
	r4 <e g>2
	
	r4 <d f>2
	r4 <bes, g>2
	r4 <c f>2
	r4 <c e>2
	
	\clef treble
	
	<d' a'>2.~
	<d' a'>2~ <d' a' bes'>4\fermata
	
	\clef bass
	
	R2.*18
	<d a>4\staccatissimo^\markup{\tiny \italic "con fuoco" } r2 
	<e g bes>4\staccatissimo r2 
	<d a d'>4.-> r4.
	
	\bar "|."
}

pianoLeft = {
	\set Staff.pedalSustainStyle = #'bracket
	
	\clef bass
	\key d \minor
	\time 3/4
	
	R2.*15	
	<f, bes, f>2.^\pp\fermata\sustainOn
	
	a2.^\mf\(\sustainOff\sustainOn^\markup{\halign #-0.45 \tiny \italic "animato"}
	bes2.\sustainOff\sustainOn
	a2.~\sustainOff\sustainOn
	a2.\)
	
	a4\(\sustainOff\sustainOn a a
	bes2\sustainOff\sustainOn bes4
	a2.~\sustainOff\sustainOn
	a2.\)
	
	a2.^\<\(\sustainOff\sustainOn
	bes2.\sustainOff\sustainOn
	a2\sustainOff\sustainOn g4\sustainOff\sustainOn
	a2.\sustainOff\sustainOn\)
	
	a4\sustainOff\sustainOn a a
	bes4\sustainOff\sustainOn bes bes
	a2.\sustainOff\sustainOn
	<d, d>4\sustainOff\sustainOn <e, e>4\!^\ff\fermata <f, f>4\sustainOff\sustainOn
	
	\repeat volta 2
	{
		<g, d>4->^\ff^\markup{\tiny \italic "eroico"}\sustainOff r2
		r2.
		<g, d>4-> r2
		r2.
		
		<f, c>4-> r2
		r2.
		<f, c>4-> r2
		r2.
		
		<es, bes,>4-> r2
		r2.
		<es, bes,>4-> r2
		r4 <es, bes,>4 <es, bes,>4
	}
	\alternative
	{
		{
			<d, a,>2 <d, a,>4
			<d, a,>2 <d, a,>4
			<d, a, d>2 <d, a, d>4
			<d, a, d>2 <d, a, d>4
		}
		{
			d,2.^\subMF^\>
			g,2.
			bes,2.
			<f, f>2.\!\pp\fermata
		}
	}
	
	f4^\markup{\tiny \italic "con moto"} f f
	f4 f c
	e4 e e
	e4 e a,
	
	d4 d d
	d2 d4
	c4_\( f bes
	c'4 f' g'\)
	
	<bes, f>2.
	<g, g>2 <a, g>4
	<c g>4 <c g>4 <c g>4
	<bes, f>2.
	
	f,4\( c f\)
	d,4\( a f\)
	bes,4\( f g\)
	c4\( f e
	
	<f, f>2.~\)
	<f, f>2.
	<< {f2. f2 f4 g2 g4 bes2.} \\ 
		{a,2.~ a,2~ a,4 g,2.~ g,2.} >>
	<e g c'>2.
	
	c,2.\(
	g,2.
	d,2.\)
	<g, g>2.
	
	c'2.
	<< {a2.\fermata} \\ {<f, f>2.\fermata} >>
	R2.*14
	r2 <c g bes>4\fermata
	
	<f, f>2. \time 4/4
	
	<d, d>4^\markup{\tiny \italic "bruscamente"} <d, d> <d, d> <d, d>
	<bes, bes>4 <a, a> <f, f> <d, d>~\fermata
	<d, d>1
	
	\repeat volta 2 {
		a4^\markup{\tiny \italic "dolce"}\( bes a g\)
		g\( a g f\)
		f\( g f d\)
	}
	\alternative {
			{	
				e1
			}
			{
				f4\( g a f\)
			}
	}
	
	\time 3/4
	
	d,2.\(^\markup{\halign #-0.3 \tiny \italic "molto legato"}
	a,2.
	d,2.
	c,2.
	
	d,2.
	g,2.
	f,2.
	a,2.\)
	
	a4\( f d
	d,2.\)\fermata
	R2.*2
	
	R2.*4
	
	<d, d>4-> r2
	<d, d>4-> r2
	<d, d>4-> r2
	<d, d>4-> r2
	
	<c, c>4-> r2
	<c, c>4-> r2
	<c, c>4-> r2
	<c, c>4-> r2
	
	<bes,, bes,>4-> r2
	<bes,, bes,>4-> r2	
	<a,, a,>4-> r2
	<a,, a,>2.--->
	
	<d, d>4\staccatissimo r2
	<c, c>4\staccatissimo r2
	<d, d>4\staccatissimo r2
}